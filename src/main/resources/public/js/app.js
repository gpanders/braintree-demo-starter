document.querySelectorAll('[data-toggle]').forEach(function(elem) {
    elem.addEventListener('click', function(e) {
        e.preventDefault();
        var toggle = elem.dataset && elem.dataset.toggle;
        document.querySelectorAll(toggle).forEach(function(elem) {
            if (elem.style.display === 'none') {
                elem.style.display = 'block';
            } else {
                elem.style.display = 'none';
            }
        });
    });
});