package braintree.api;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Environment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by ganders on 4/6/16.
 */

@Component
public class BraintreeClient extends BraintreeGateway {

    private static final Logger LOG = Logger.getLogger(BraintreeClient.class);

    @Autowired
    public BraintreeClient(@Value("${braintree.merchant.id}") String merchantId,
                           @Value("${braintree.public.key}") String publicKey,
                           @Value("${braintree.private.key}") String privateKey) {

        super(
                Environment.SANDBOX,
                merchantId,
                publicKey,
                privateKey
        );
    }
}
