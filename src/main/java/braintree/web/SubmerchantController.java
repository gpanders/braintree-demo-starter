package braintree.web;

import braintree.api.BraintreeClient;
import braintree.domain.Submerchant;
import com.braintreegateway.MerchantAccount;
import com.braintreegateway.MerchantAccountRequest;
import com.braintreegateway.Result;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ganders on 5/18/16.
 */

@Component
@RequestMapping("/submerchant")
public class SubmerchantController {

    private static final String MASTER_MERCHANT_ACCOUNT_ID = "credera";

    private static final Logger LOG = Logger.getLogger(SubmerchantController.class);

    @Autowired
    private BraintreeClient client;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        return "submerchants/signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity signup(@ModelAttribute Submerchant submerchant) {
        MerchantAccountRequest request = new MerchantAccountRequest().
                individual().
                    firstName(submerchant.getFirstName()).
                    lastName(submerchant.getLastName()).
                    email(submerchant.getEmail()).
                    phone(submerchant.getPhone()).
                    dateOfBirth(submerchant.getDateOfBirth()).
                    ssn(submerchant.getSsn()).
                    address().
                        streetAddress(submerchant.getStreetAddress()).
                        locality(submerchant.getLocality()).
                        region(submerchant.getRegion()).
                        postalCode(submerchant.getPostalCode()).
                        done().
                    done().
                business().
                    legalName(submerchant.getBusinessName()).
                    taxId(submerchant.getTaxId()).
                    done().
                funding().
                    destination(MerchantAccount.FundingDestination.BANK).
                    accountNumber("1123581321").
                    routingNumber("071101307").
                    done().
                tosAccepted(submerchant.getTosAccepted()).
                masterMerchantAccountId(MASTER_MERCHANT_ACCOUNT_ID);

        Result<MerchantAccount> result = client.merchantAccount().create(request);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
