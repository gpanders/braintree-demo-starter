package braintree.web;

import braintree.api.BraintreeClient;
import braintree.domain.CustomerForm;
import braintree.service.CustomerService;
import com.braintreegateway.Customer;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by ganders on 5/19/16.
 */
@Component
@RequestMapping("/customers")
public class CustomersController {

    @Autowired
    private BraintreeClient client;

    @Autowired
    private CustomerService customerService;

    @ModelAttribute("customers")
    public List<Customer> getCustomers() {
        customerService.find("abc").getDefaultPaymentMethod().get
        return customerService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        return "customer/customer";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String index(@Valid @ModelAttribute CustomerForm customerForm, Model model) {
        CustomerRequest request = new CustomerRequest()
                .firstName(customerForm.getFirstName())
                .lastName(customerForm.getLastName())
                .company(customerForm.getCompany())
                .email(customerForm.getEmail())
                .fax(customerForm.getFax())
                .phone(customerForm.getPhone())
                .website(customerForm.getWebsite());

        Result<Customer> result = client.customer().create(request);

        model.addAttribute("success", result.isSuccess());
        if (result.isSuccess()) {
            Customer customer = result.getTarget();
            customerService.save(customer);
        }

        return "customer/customer";
    }
}
