package braintree.web;

import braintree.api.BraintreeClient;
import braintree.domain.CustomerForm;
import braintree.service.CustomerService;
import com.braintreegateway.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * Created by ganders on 4/25/16.
 */

@Controller
@RequestMapping("/")
public class HomeController {

    private static final Logger LOG = Logger.getLogger(HomeController.class);

    @Autowired
    private BraintreeClient client;

    @Autowired
    private CustomerService customerService;

    @ModelAttribute("clientToken")
    public String getClientToken() {
        return client.clientToken().generate();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "checkout", method = RequestMethod.GET)
    public String redirectToIndex() {
        return "redirect:/";
    }

    @RequestMapping(value = "checkout", method = RequestMethod.POST)
    public String checkout(@Valid @ModelAttribute CustomerForm customerForm,
                           @RequestParam("payment_method_nonce") String nonce,
                           @RequestParam String amount,
                           Model model) {
        LOG.info(String.format("Received payment method nonce %s", nonce));



        CustomerRequest customerRequest = new CustomerRequest()
                .firstName(customerForm.getFirstName())
                .lastName(customerForm.getLastName())
                .company(customerForm.getCompany())
                .email(customerForm.getEmail())
                .fax(customerForm.getFax())
                .phone(customerForm.getPhone())
                .website(customerForm.getWebsite())
                .paymentMethodNonce(nonce);

        Result<Customer> customerResult = client.customer().create(customerRequest);

        if (customerResult.isSuccess()) {
            customerService.save(customerResult.getTarget());

            TransactionRequest transactionRequest = new TransactionRequest()
                .amount(new BigDecimal(amount))
                .paymentMethodNonce(nonce)
                .options()
                .submitForSettlement(true)
                .done();

            Result<Transaction> transactionResult = client.transaction().sale(transactionRequest);

            return "redirect:/customers";
        }

        model.addAttribute("error", true);
        return "index";
    }
}
