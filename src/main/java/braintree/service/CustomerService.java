package braintree.service;

import braintree.api.BraintreeClient;
import com.braintreegateway.Customer;
import com.braintreegateway.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ganders on 5/19/16.
 */
@Service
public class CustomerService {

    @Autowired
    private BraintreeClient client;

    private List<Customer> customers = new ArrayList<>();

    public void save(Customer customer) {
        customers.removeIf(c -> c.getId().equals(customer.getId()));
        customers.add(customer);
    }

    public Customer delete(String id) {
        Result<Customer> result = client.customer().delete(id);
        if (result.isSuccess()) {
            Customer customer = result.getTarget();
            customers.removeIf(c -> c.getId().equals(id));
            return customer;
        }
        return null;
    }

    public Customer find(String id) {
        return client.customer().find(id);
    }

    public List<Customer> getAll() {
        return customers;
    }
}
