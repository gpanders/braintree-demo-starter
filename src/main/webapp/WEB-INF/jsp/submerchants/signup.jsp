<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="/WEB-INF/jsp/partials/head.jsp" />
<body>
    <div class="container">
        <form id="submerchant-signup" name="submerchant" action="/submerchant/signup" method="POST">
            <label>
                First Name
                <input type="text" name="firstName" value="Jane" />
            </label>

            <label>
                Last Name
                <input type="text" name="lastName" value="Doe" />
            </label>

            <label>
                Email
                <input type="email" name="email" value="jane@14ladders.com" />
            </label>

            <label>
                Phone
                <input type="text" name="phone" value="5553334444" />
            </label>

            <label>
                Date of Birth
                <input type="date" name="dateOfBirth" value="1981-11-19" />
            </label>

            <label>
                SSN
                <input type="password" name="ssn" value="456-45-4567" />
            </label>

            <label>
                Street Address
                <input type="text" name="streetAddress" value="111 Main St" />
            </label>

            <label>
                Locality
                <input type="text" name="locality" value="111 Main St" />
            </label>

            <label>
                State
                <input type="text" name="region" maxlength="2" value="CO" />
            </label>

            <label>
                ZIP Code
                <input type="text" name="postalCode" value="80922" />
            </label>

            <label>
                Business Name
                <input type="text" name="businessName" value="Jane's Ladders" />
            </label>

            <label>
                Tax ID
                <input type="text" name="taxId" value="98-7654321" />
            </label>

            <label>
                I have read and agree to the Terms of Service
                <input type="checkbox" name="tosAccepted" required="required" />
            </label>

            <button class="btn" type="submit">Submit</button>
        </form>
    </div>

    <jsp:include page="/WEB-INF/jsp/partials/scripts.jsp" />
</body>
</html>