<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<jsp:include page="/WEB-INF/jsp/partials/head.jsp" />
<body>
    <a href="/">&lt; Home</a>
    <c:if test="${not empty customers}">
        <h4>Current customers:</h4>
        <c:forEach items="${customers}" var="customer">
            <div class="customer">
                <div class="id">${customer.id}</div>
                <div class="name">${customer.firstName} ${customer.lastName}</div>
                <div class="email">${customer.email}</div>
                <div class="payment-method">
                    <img src="${customer.defaultPaymentMethod.imageUrl}" />
                </div>
            </div>
        </c:forEach>
    </c:if>
    <div class="container">
        <h4>Create a new customer</h4>
        <form id="create-customer" name="customer" action="/customer" method="POST">
            <label>
                First Name
                <input type="text" name="firstName" value="" required />
            </label>

            <label>
                Last Name
                <input type="text" name="lastName" value="" required />
            </label>

            <label>
                Email
                <input type="email" name="email" value="" required />
            </label>

            <label>
                Phone
                <input type="text" name="phoneNumber" value="" />
            </label>

            <label>
                Fax Number
                <input type="text" name="faxNumber" value="" />
            </label>

            <label>
                Website
                <input type="text" name="website" value="" />
            </label>

            <label>
                Company
                <input type="text" name="company" value="" />
            </label>

            <button class="btn" type="submit">Submit</button>
        </form>
    </div>

    <jsp:include page="/WEB-INF/jsp/partials/scripts.jsp" />
</body>
</html>