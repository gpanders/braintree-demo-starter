<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<jsp:include page="partials/head.jsp" />
<body>
    <div class="container">
        <a href="/customers">Customers</a>
        <a href="/submerchant">Submerchants</a>

        <c:if test="${error eq true}">
            <h1>Something went wrong!</h1>
        </c:if>

        <h4>Create a new customer</h4>
        <div id="dropin-container"></div>
        <form id="checkout-form" method="POST" action="/checkout">
            <label for="card-number">Card Number</label>
            <div id="card-number" class="hosted-field"></div>

            <label for="cvv">CVV</label>
            <div id="cvv" class="hosted-field"></div>

            <label for="expiration-date">Expiration Date</label>
            <div id="expiration-date" class="hosted-field"></div>

            <label>
                Payment Amount
                <input id="amount-slider" type="range" min="1" max="1000" step="1" />
                <input id="amount-text" type="text" name="amount" required />
            </label>
            <label>
                First Name
                <input type="text" name="firstName" value="" required />
            </label>

            <label>
                Last Name
                <input type="text" name="lastName" value="" required />
            </label>

            <label>
                Email
                <input type="email" name="email" value="" required />
            </label>

            <label>
                Phone
                <input type="text" name="phoneNumber" value="" />
            </label>

            <label>
                Fax Number
                <input type="text" name="faxNumber" value="" />
            </label>

            <label>
                Website
                <input type="text" name="website" value="" />
            </label>

            <label>
                Company
                <input type="text" name="company" value="" />
            </label>
            <input type="submit" value="Submit" />
        </form>

        <section class="spacer"></section>

        <button type="button" class="mb" data-toggle="#credit-cards">Show/Hide Test Cards</button>
        <div id="credit-cards" style="display:none">
            <table>
                <thead>
                    <tr>
                        <th>Test Value</th>
                        <th>Card Type</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">378282246310005</code></td>
                        <td>American Express</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">371449635398431</code></td>
                        <td>American Express</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">6011111111111117</code></td>
                        <td>Discover</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">3530111333300000</code></td>
                        <td>JCB</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">6304000000000000</code></td>
                        <td>Maestro</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">5555555555554444</code></td>
                        <td>Mastercard</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4111111111111111</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4005519200000004</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4009348888881881</code></td>
                        <td>Visa  </td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4012000033330026</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4012000077777777</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4012888888881881</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4217651111111119</code></td>
                        <td>Visa</td>
                    </tr>
                    <tr>
                        <td><code class="syntax-inline syntax-inline--theme">4500600000000061</code></td>
                        <td>Visa</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <jsp:include page="partials/scripts.jsp" />
    <script type="text/javascript">
        var clientToken = '${clientToken}';

        braintree.setup(clientToken, 'custom', {
            id: 'checkout-form',
            hostedFields: {
                number: {
                    selector: '#card-number'
                },
                cvv: {
                    selector: '#cvv'
                },
                expirationDate: {
                    selector: '#expiration-date'
                }
            }
        });

        var slider = document.getElementById('amount-slider');
        var amount = document.getElementById('amount-text');

        slider.addEventListener('input', function(event) {
            amount.value = this.value;
        });

        amount.addEventListener('change', function(event) {
            slider.value = this.value;
        });
    </script>
</body>
</html>